require 'sinatra'
require 'net/http'
require 'uri'

def get_data
  uri = URI.parse('https://api.clickbank.com/rest/1.3/orders/list')
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  req = Net::HTTP::Get.new(uri.request_uri)
  req['Authorization'] = 'DEV-EB2FEODKSQ65E6V55MAC3540BK1AGJ4F:API-K6O7BB81DJ5N1O0QDF5P58L767M0A6BS'
  http.request(req)
end

get '/' do
  'clickbank-api proxy server'
end

[:get, :options].each do |x|
  send(x, '/clickbank-api/rest/1.3/orders/list') do
    res = get_data
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = "GET, POST, PUT, DELETE, OPTIONS"
    response['Access-Control-Allow-Headers'] ="accept, authorization, origin"
    res.body
  end
end



# get '/clickbank-api/rest/1.3/orders/list' do
#   res = get_data
#   response['Access-Control-Allow-Origin'] = '*'
#   response['Access-Control-Allow-Methods'] = "GET, POST, PUT, DELETE, OPTIONS"
#   response['Access-Control-Allow-Headers'] ="accept, authorization, origin"
#   res.body
# end

# options '/clickbank-api/rest/1.3/orders/list' do
#   res = get_data
#   response['Access-Control-Allow-Origin'] = '*'
#   response['Access-Control-Allow-Methods'] = "GET, POST, PUT, DELETE, OPTIONS"
#   response['Access-Control-Allow-Headers'] ="accept, authorization, origin"
#   res.body
# end